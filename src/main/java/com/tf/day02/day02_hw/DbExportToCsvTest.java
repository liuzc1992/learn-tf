package com.tf.day02.day02_hw;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import utils.JDBCUtils;

import javax.sql.DataSource;

import org.junit.Test;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.hamcrest.CoreMatchers.nullValue;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class DbExportToCsvTest {
	 private  JdbcTemplate jdbcTemplate = new JdbcTemplate(new ComboPooledDataSource("localhost"));
//	 private  JdbcTemplate jdbcDuridTemplate = new JdbcTemplate(JDBCUtils.getDataSource());
	 
    
    @Test
    public void test1() {
    	String sql = "update student set age=? where id=?";
        int update = jdbcTemplate.update(sql, 101, 2);
        System.out.println(update);
    }
    @Test
    public void test2() {
    	String sql = "update student set age=? where id=?";
        int update = jdbcTemplate.update(sql, 102, 2);
        System.out.println(update);
    }
    
    @Test
    public void test3() throws IOException {
    	String sql = "select * from student ";
        List<Map<String,Object>> list = jdbcTemplate.queryForList(sql);
//        FileOutputStream fos = new FileOutputStream("D:\\Test\\daochu.csv");
        FileWriter fileWriter = new FileWriter("D:\\Test\\daochu.csv");
       BufferedWriter bf=new  BufferedWriter(new FileWriter("D:\\Test\\daochu.txt"));
        for(Map<String, Object> map:list) {
        	String str=String.valueOf(map);
        	try {
				bf.write(str);
				bf.newLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	System.out.println(str);
        }
//        System.out.println(update);
    }
    
  

}
