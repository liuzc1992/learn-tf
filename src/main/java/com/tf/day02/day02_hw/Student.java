package com.tf.day02.day02_hw;

import java.io.Serializable;
import java.util.Date;

public class Student implements Serializable {
	private Integer id;
	private String name;
	private Integer age;
	private Double score;
	private Date birthday;
	private Date insert_time;
	
	
	
	public Student(Integer id, String name, Integer age, Double score, Date birthday, Date insert_time) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.score = score;
		this.birthday = birthday;
		this.insert_time = insert_time;
	}
	public Student() {
		super();
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + ", score=" + score + ", birthday=" + birthday
				+ ", insert_time=" + insert_time + "]";
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		this.score = score;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public Date getInsert_time() {
		return insert_time;
	}
	public void setInsert_time(Date insert_time) {
		this.insert_time = insert_time;
	}
	
	

}
