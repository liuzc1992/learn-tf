package com.tf.day02.day02_hw;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class DbExportText {

	public static void main(String[] args) throws IOException {
			test4();
	}
	
    public static void test4() throws IOException {
    	JdbcTemplate jdbcTemplate = new JdbcTemplate(new ComboPooledDataSource("localhost"));
    	ObjectOutputStream ops=null;
    	ops=new ObjectOutputStream(new FileOutputStream("D:\\Test\\daochu.txt"));
    	String sql = "select * from student ";
    	List<Student> list=jdbcTemplate.query(sql,new 	BeanPropertyRowMapper<Student>(Student.class));
    	for(Student student:list) {
    		try {
				String string="id:"+student.getId()+"  name:"+student.getName()
    			+"  score:"+student.getScore()+"  Birthday:"+student.getBirthday()+"  Insert_time:"+student.getInsert_time();
				
				System.out.println("id:"+student.getId()+"  name:"+student.getName());
    			ops.writeObject(string+"\r\n");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
}
