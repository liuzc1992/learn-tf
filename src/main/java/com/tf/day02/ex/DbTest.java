//package com.tf.day02.ex;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//
//public class DbTest {
//
//	public static void main(String[] args) {
//		//找一个工具去连接数据库
//		Connection conn = null;
//		Statement statement = null;
//		try {
//			Class.forName("com.mysql.jdbc.Driver");
//			//连接数据库
//			conn = DriverManager.getConnection
//			("jdbc:mysql://118.24.13.38:3308/test?characterEncoding=utf8&useSSL=false", "zhangsan", "123123");
//
//			//准备sql
//			String  sql= "select * from t_user_test";
//
//			//创建执行sql对象
//			statement = conn.createStatement();
//			//执行sql, 获取返回结果集合
//			ResultSet  set=statement.executeQuery(sql);
//			while (set.next()) {
//				System.out.println(set.getString("uid")+" "+set.getString("loginname")+" "+set.getString("loginpass"));
//		    }
//		} catch (Exception e) {
//			e.printStackTrace();
//		}finally {
//			//数据库关闭
//			try {
//				conn.close();
//				statement.close();
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		}
//	}
//
//}
