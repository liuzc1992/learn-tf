package com.tf.day02.ex.db;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbTest1 {
	public static void main(String[] args) {
		//鎵句竴涓伐鍏峰幓杩炴帴鏁版嵁搴�
		Connection conn = null;
		Statement statement = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//杩炴帴鏁版嵁搴�
			conn = DriverManager.getConnection
			("jdbc:mysql://127.0.0.1:3306/liuzc?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC", "root", "123456");
			
			//鍑嗗sql
			String  sql= "select * from student";
			
			//鍒涘缓鎵цsql瀵硅薄
			statement = conn.createStatement();
			//鎵цsql, 鑾峰彇杩斿洖缁撴灉闆嗗悎
			ResultSet  set=statement.executeQuery(sql);
			while (set.next()) {
				System.out.println(set.getString("id")+" "+set.getString("name")+" "+set.getString("score")+set.getString("birthday")+set.getString("insert_time"));
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			//少食多餐v
			try {
				conn.close();
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
