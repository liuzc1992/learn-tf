package com.tf.day03.ex;

import org.apache.commons.dbutils.QueryRunner;
import org.junit.Before;
import utils.JDBCC3p0Utils;
import utils.JDBCUtils;

import java.sql.SQLException;
import java.util.UUID;

public class TestDbUtils2 {
    private static QueryRunner runner = new QueryRunner(JDBCC3p0Utils.getDataSource());

    public static void main(String[] args) throws SQLException {
//       testUpdate();
//        testInsert();
//        testDelete();

//        for (int i = 1; i <2000 ; i++) {
//            String sql = "insert into t_user_test values(?,?,?)";
//            Object[] objects = new Object[]{String.valueOf(UUID.randomUUID()), "name"+i, "pwd"+i};
//            int update = runner.update(sql, objects);
//            System.out.println("insert:"+update);
//        }

//        test("1","ww");

        test(new String[]{"serfwe", "serfwe2", "serfwe3", "serfwe4", "serfwe5"});
    }

    private static void testDelete() throws SQLException {
        String sql = "delete from t_user_test";
        Object object = new Object[]{"id13"};
        runner.update(sql);
    }

    private static void testInsert() throws SQLException {
        String sql = "insert into t_user_test values(?,?,?)";
        Object[] objects = new Object[]{"id123", "zss", "1234"};
        int update = runner.update(sql, objects);
        System.out.println("insert:" + update);


    }

    public static void testUpdate() throws SQLException {
        String sql = "update t_user_test set loginname=? , loginpass=? where uid=?";
        Object[] objects = new Object[]{"lzc", "12345677799", "1"};
        int update = runner.update(sql, objects);
        System.out.println(update);

    }

    private static void test(String... o) {

        for (String string : o) {
            System.out.println(string);
        }
    }
}


