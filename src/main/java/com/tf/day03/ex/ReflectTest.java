package com.tf.day03.ex;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.MethodUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectTest {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        Object object = null;

        //class获取方式1
            Class clzClass = Class.forName("com.tf.day03.ex.Dbuser");
            //类已经加载了的话  选择newInstance()  可以new对象;
            object = clzClass.newInstance();
        if (object instanceof Dbuser) {
            System.out.println("ok");
        }

        //class获取方式2
        Class dbuserClass = Dbuser.class;

        //class获取方式3  通过对象.getClass方式获去class
        Dbuser dbuser = new Dbuser();
        Class aClass = dbuser.getClass();

        //有了class  可以用来干嘛？、
//        一个对象的组成有  Class（类）  Field(属性)  Method(方法)？
        //有了class后  可以获取类的属性  方法

        //利用class  获取类中的多有方法和属性

        Class clzClass3 = object.getClass();
        Field[] declaredField = clzClass3.getDeclaredFields();
        for (Field field : declaredField) {
            System.out.println(field.getName());
        }

        //利用class属性获取类下面的所有方法
        Method[] methods = clzClass.getMethods();
        for (Method method : methods) {
            System.out.println(method.getName());
        }

        //利用BeanUtils.setProperty 对对象中的属性赋值
        Dbuser dbuser1 = new Dbuser();
        BeanUtils.setProperty(dbuser1,"uid","123456");  //类：dbuser1     属性：uid    值："123456"
        //利用这个方法可以直接对类中的属性进行赋值
        System.out.println(dbuser1);

        MethodUtils.invokeMethod(dbuser1,"setLoginname","zlc");//对方法进行赋值
        System.out.println(dbuser1);

    }
}
