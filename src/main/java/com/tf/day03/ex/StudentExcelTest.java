package com.tf.day03.ex;

import com.github.crab2died.annotation.ExcelField;

public class StudentExcelTest {
    @ExcelField(title="header1")
    private String id;

    @ExcelField(title="header2")
    private String name;

    @ExcelField(title="header3")
    private String addr;


    @ExcelField(title="header4")
    private String agender;

    @ExcelField(title="header6")
    private String phone;


    @Override
    public String toString() {
        return "StudentExcelTest{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", addr='" + addr + '\'' +
                ", agender='" + agender + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getAgender() {
        return agender;
    }

    public void setAgender(String agender) {
        this.agender = agender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
