package com.tf.day03.ex;

public class Dbuser {
    private String uid;
    private String loginname;
    private String loginpass;

    public Dbuser() {
    }

    public Dbuser(String uid, String loginname, String loginpass) {
        this.uid = uid;
        this.loginname = loginname;
        this.loginpass = loginpass;
    }

    @Override
    public String toString() {
        return "Dbuser{" +
                "uid='" + uid + '\'' +
                ", loginname='" + loginname + '\'' +
                ", loginpass='" + loginpass + '\'' +
                '}';
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    public String getloginpass() {
        return loginpass;
    }

    public void setloginpass(String loginpass) {
        this.loginpass = loginpass;
    }
}
