package com.tf.day03.ex;

import com.github.crab2died.ExcelUtils;
import com.github.crab2died.exceptions.Excel4JException;
import com.tf.day03.course.excel4j.StudentExcel;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class TestExcel4j {
    public static void main(String[] args) throws InvalidFormatException, Excel4JException, IOException {
        //定义excel的路径
        String path = System.getProperty("user.dir") + File.separator + "data" + File.separator + "test.xlsx";
        //读取excel
        List<StudentExcelTest> stu = ExcelUtils.getInstance().readExcel2Objects(path, StudentExcelTest.class);
        System.out.println(stu);

        //写Excel---传一个list    对象.class  excel的目标路径和名称
        ExcelUtils.getInstance().exportObjects2Excel(stu,StudentExcelTest.class,"res.xlsx");



    }
}
