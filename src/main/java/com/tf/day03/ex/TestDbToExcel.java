package com.tf.day03.ex;

import com.github.crab2died.ExcelUtils;
import com.github.crab2died.exceptions.Excel4JException;
import com.tf.day03.course.utills.JDBCC3p0Utils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import sun.security.krb5.internal.tools.Klist;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class TestDbToExcel {
    private static QueryRunner runner = new QueryRunner(JDBCC3p0Utils.getDatatSource());

    public static void main(String[] args) throws SQLException, Excel4JException, IOException {

        List list = testQueryAll();

        DbToExcel(list);
    }

    public static List testQueryAll() throws SQLException, Excel4JException, IOException {
        String sql = "select * from t_user_test";
        //将查询出来的数据强制转为List   再将list传给后面的
        List list = (List) runner.query(sql, new BeanListHandler(Dbuser2.class));
        System.out.println(list.size());
        return list;

    }
    private static void DbToExcel(List list) throws Excel4JException, IOException {
        //将Db的数据利用Dbuser2这个对象 将db对应的表中的每条数据封装为一个对象 存在list中  将list导入到excel中
        ExcelUtils.getInstance().exportObjects2Excel(list, Dbuser2.class, "res_Dbuser.xlsx");
    }
}
