package com.tf.day03.ex;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import utils.JDBCC3p0Utils;

import java.sql.SQLException;
import java.util.List;

public class TestDbUtilsQuery {
    private static QueryRunner runner = new QueryRunner(JDBCC3p0Utils.getDataSource());
    public static void main(String[] args) {
        testQuery();
    }

    private static void testQuery()  {
        String sql = "select * from t_user_test";
//        runner.q;
        List list = null;

        //反射        类.class可以获取类中的所有属性和方法
        //1.//class获取方式2
        //Class dbuserClass = Dbuser.class;  // 有了class就有了类中的所有属性  就能创建对象
        //2.然后会创建对象，
        //3.//利用BeanUtils.setProperty 对对象中的属性赋值
        //Dbuser dbuser1 = new Dbuser();
        //BeanUtils.setProperty(dbuser1,"uid","123456");
        //System.out.println(dbuser1);
        //
        //从而实现是需要对象.class就能对对象中的所有属性进行赋值的操作。

        try {
            list = (List) runner.query(sql, new BeanListHandler(Dbuser.class));//反射
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(list);
        for (Object object : list) {
            System.out.println(list);
        }
    }

}
