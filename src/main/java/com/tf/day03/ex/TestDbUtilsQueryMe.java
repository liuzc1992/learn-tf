package com.tf.day03.ex;

import com.tf.day03.course.utills.JDBCC3p0Utils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TestDbUtilsQueryMe {
    private static QueryRunner runner = new QueryRunner(JDBCC3p0Utils.getDatatSource());

    public static void main(String[] args) throws SQLException {
//        testSelectAll();
//        testQuery1();
//        testQueryMap();
        testQueryMapAll();

    }

    private static void testSelectAll() throws SQLException {
        String sql = "select * FROM t_user_test";
        List list = (List) runner.query(sql, new BeanListHandler(Dbuser.class));
//        System.out.println(query);
        for (Object list1 : list) {
            System.out.println(list1);
        }
    }

    private static void testQuery1() throws SQLException {
        String sql = "select * from t_user_test where loginname=?";
        Dbuser user = (Dbuser) runner.query(sql, new BeanHandler(Dbuser.class), "name10");
        System.out.println(user.getUid() + "\t" + user.getLoginname() + "\t" + user.getloginpass());
    }

    private static void testQueryMap() throws SQLException {
        String sql = "select * from t_user_test where loginname=?";
        Map<String, Object> query = runner.query(sql, new MapHandler(),"name1");
        System.out.println(query);

    }
    private static void testQueryMapAll() throws SQLException {
        String sql = "select * from t_user_test";
        List<Map<String, Object>> query = runner.query(sql, new MapListHandler());
        for (Map<String, Object> map : query) {
            Set<String> keys = map.keySet();
            for (String key : keys) {
                System.out.println(key+"\t"+map.get(key));

            }
        }

    }

}
