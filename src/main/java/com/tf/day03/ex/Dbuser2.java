package com.tf.day03.ex;

import com.github.crab2died.annotation.ExcelField;

public class Dbuser2 {
    @ExcelField(title = "用户id")
    private String uid;

    @ExcelField(title = "用户名字")
    private String loginname;

    @ExcelField(title = "用户密码")
    private String loginpass;


    @Override
    public String toString() {
        return "Dbuser2{" +
                "uid='" + uid + '\'' +
                ", loginname='" + loginname + '\'' +
                ", loginpass='" + loginpass + '\'' +
                '}';
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    public String getLoginpass() {
        return loginpass;
    }

    public void setLoginpass(String loginpass) {
        this.loginpass = loginpass;
    }
}
