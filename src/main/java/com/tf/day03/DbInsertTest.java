package com.tf.day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;

import javax.sql.DataSource;
import javax.xml.transform.Templates;

import com.mchange.v2.c3p0.ComboPooledDataSource;


public class DbInsertTest {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		TestInsertSql();
	}
	public static void TestInsertSql() throws Exception {
		Random random= new Random();
		int t=random.nextInt(1000);
		DataSource ds=new ComboPooledDataSource("localhost");
		Connection conn=ds.getConnection();
//		("name","score","birthday","insert_time")
		String sql="insert into student(name,score,birthday,insert_time) values(?,?,?,?)";
		
		PreparedStatement pstmt=conn.prepareStatement(sql);
		pstmt.setString(1, "name1"+t);
		pstmt.setInt(2, t);
		pstmt.setString(3, null);
		pstmt.setString(4, null);
		pstmt.execute(sql);
		pstmt.close();
		conn.close();
		
	}

}
