package com.tf.day03.course.utills;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class JDBCC3p0Utils {
	public static DataSource getDatatSource() {
		DataSource ds=new ComboPooledDataSource();
		return ds;
	}
	
	public static Connection getConnection(DataSource ds) {
		Connection conn=null;
		try {
			conn = ds.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
		
	}

//	public static void main(String[] args) {
//		getConnection(getDatatSource());
//	}

}
