package com.tf.day03.course.dbutils;

import com.testfan.dbutils.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;

import java.sql.SQLException;
import java.util.UUID;

// 参考例子
//https://blog.csdn.net/a911711054/article/details/77719685
public class DbUtilsTest {

	public static void main(String[] args) {
//		try {
//			update();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		
		
		try {
			add();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	private static void update() throws SQLException {
		//ComboPooledDataSource ds = new ComboPooledDataSource();
		// dbutis使用数据源
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		// 可变变量  无限 也可以没有 也可以数组
		Object[] objects= new Object[] {"123_test","333_test", "14ba4bd0-a0da-4a2c-b136-de036b54e98a"};
		//runner.update("update t_user_test set loginname=?,loginpass=? where uid=?", "123_dbutils","123_dbutils","14ba4bd0-a0da-4a2c-b136-de036b54e98a");
		runner.update("update t_user_test set loginname=?,loginpass=? where uid=?", objects);
		runner.update("delete from t_user_test");
	}
	
	private static void add() throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		runner.update("delete from t_user_test");
		for (int i = 0; i < 1000; i++) {
			Object[] objects= new Object[] {UUID.randomUUID().toString(),"test"+i, "pass"+i};
			runner.update("insert INTO t_user_test(uid,loginname,loginpass) values(?,?,?)", objects);
		}
	}
	
	
	private static void test(String...o) {
		for (String string : o) {
			System.out.println(string);
		}
	}

}
