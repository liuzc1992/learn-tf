package com.tf.day03.course.dbutils;

import com.testfan.dbutils.JDBCUtils;
import com.tf.day03.course.db.DbUser;
import com.tf.day03.ex.Dbuser;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

//https://blog.csdn.net/a911711054/article/details/77719685
//参考例子

public class DbutilsQueryTest {
	public static void main(String[] args) {
		query();
	}
	
	private static void query() {
		// dbutis使用数据源
 		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		String sql = "select * from t_user_test";
		try {
	 		// 有对象反射
			//获取多条数据用BeanListHandler  获取一条数据用BeanHandler
			List<DbUser> list = runner.query(sql, new BeanListHandler<>(DbUser.class));
			Dbuser query = runner.query(sql, new BeanHandler<>(Dbuser.class));
			System.out.println(list);
			for (DbUser dbUser : list) {
				System.out.print(dbUser.getLoginname()+" ");
				System.out.print(dbUser.getLoginpass());
				System.out.println();
			}
			DbUser dbUser =  (DbUser) runner.query("select * from t_user_test where uid=?", new BeanHandler<>(DbUser.class),"21e25344-e8df-4346-8196-bfab7ab26958");
			System.out.println(dbUser);
			//没有对象
			Map<String, Object> mapobject =runner.query("select * from t_user_test where uid=?",new MapHandler(),"21e25344-e8df-4346-8196-bfab7ab26958");
			System.out.println(mapobject);
			List<Map<String, Object>> listmap = runner.query("select * from t_user_test", new MapListHandler());
			System.out.println(listmap);
			System.out.println();
//			System.out.println(list);
			for (Map<String, Object> map : listmap) {
			  Set<String> keys = map.keySet();
			  for (String key : keys) {
				  System.out.println(key +" "+map.get(key)+" \t");
			  }
			  System.out.println();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


}
