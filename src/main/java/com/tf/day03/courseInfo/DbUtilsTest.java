package com.tf.day03.courseInfo;

import java.sql.SQLException;
//import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.mchange.v2.c3p0.ComboPooledDataSource;


public class DbUtilsTest {

	public static void main(String[] args) {
//		try {
//			update();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		
		test("1","2","11");
		test();
		test(new String[] {"1","1","11212"});
		
//		try {
//			add();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
	}

	private static void query() {
		// 数据的管理
		ComboPooledDataSource ds = new ComboPooledDataSource();
		// dbutis使用数据源
		QueryRunner runner = new QueryRunner(ds);
		String sql = "select * from t_user_test";
		try {
			// 反射
			List list = (List) runner.query(sql, new BeanListHandler(DbUser.class));
			// 删除，修改，add
			System.out.println(list);
			for (Object object : list) {
				System.out.println(list);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void update() throws SQLException {
		//ComboPooledDataSource ds = new ComboPooledDataSource();
		// dbutis使用数据源
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		// 可变变量  无限 也可以没有 也可以数组
		Object[] objects= new Object[] {"123_test","333_test", "14ba4bd0-a0da-4a2c-b136-de036b54e98a"};
		//runner.update("update t_user_test set loginname=?,loginpass=? where uid=?", "123_dbutils","123_dbutils","14ba4bd0-a0da-4a2c-b136-de036b54e98a");
		runner.update("update t_user_test set loginname=?,loginpass=? where uid=?", objects);
		runner.update("delete from t_user_test");
	}
	
	private static void add() throws SQLException {
		QueryRunner runner = new QueryRunner(JDBCUtils.getDataSource());
		runner.update("delete from t_user_test");
		for (int i = 0; i < 1000; i++) {
			Object[] objects= new Object[] {UUID.randomUUID().toString(),"test"+i, "pass"+i};
			runner.update("insert INTO t_user_test(uid,loginname,loginpass) values(?,?,?)", objects);
		}
	}
	
	
	private static void test(String...o) {
		for (String string : o) {
			System.out.println(string);
		}
	}

}
