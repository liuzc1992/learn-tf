//package com.tf.day03.courseInfo;
//
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.List;
//
//import com.tf.day02.day02_hw.Student;
//
//public class DbTest {
//
//	public static void main(String[] args) throws Exception {
//		List<Student> list = getAllStudentTest();
//		System.out.println(list.size());
//		//dbtoFilebystream(list);
//		dbtoFilebywriter(list);
//		System.out.println(" 结束了 ");
////		for (Student Student : list) {
////			System.out.println(Student);
////		}
//	}
//
//	private static void dbtoFilebystream(List<Student> list) throws Exception {
//		String path="db1.csv";
//		FileOutputStream fos=new FileOutputStream(path, true);
//		for (Student Student : list) {
//			fos.write(Student.toString().getBytes());
//			System.out.println(Student);
//		}
//		fos.close();
//	}
//
//	private static void dbtoFilebywriter(List<Student> list) throws Exception {
//		String path="db2.csv";
//		 FileWriter fw=new FileWriter(path, true);
//		for (Student Student : list) {
//			fw.write(Student.toString());
//		}
//		fw.close();
//	}
//
//	//jdk 原始代码
//	public static List<Student> getAllStudentTest(){
//		Connection conn = null;
//		Statement statement = null;
//		List<Student> list =null;
//		try {
//			//找一个工具去连接数据库
//			Class.forName("com.mysql.cj.jdbc.Driver");
//			//连接数据库
//			conn = DriverManager.getConnection
//			("jdbc:mysql://118.24.13.38:3308/test?characterEncoding=utf8&useSSL=false", "zhangsan", "123123");
//
//			//准备sql
//			String  sql= "select * from t_user_test";
//
//			//创建执行sql对象
//			statement = conn.createStatement();
//			//执行sql, 获取返回结果集合
//			ResultSet  rs=statement.executeQuery(sql);
//			list = new ArrayList<Student>();
//			while (rs.next()) {
//				Student user = new Student();
//				user.setAge(rs.getInt("id"));
//				user.setName(rs.getString("name"));
//				user.setScore(rs.getDouble("score"));
//				list.add(user);
//				//System.out.println(set.getString("uid")+" "+set.getString("loginname")+" "+set.getString("loginpass"));
//		    }
//		} catch (Exception e) {
//			e.printStackTrace();
//		}finally {
//			//数据库关闭
//			try {
//				conn.close();
//				statement.close();
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		}
//		return list;
//	}
//
//}
