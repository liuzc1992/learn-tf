package com.tf.day04.course;

public class HttpClientGet {

	public static void main(String[] args){
		String resultString = HttpClientUtils.doGet("http://www.baidu.com","userId=123");
         System.out.println(resultString);
         
 		 resultString = HttpClientUtils.doGet("http://www.baidu.com","userId=123;token=133");
 		 System.out.println(resultString);
         
         resultString= HttpClientUtils.doGet("http://118.24.13.38:8080/goods/UserServlet?method=loginMobile&loginname=test1&loginpass=test1");
         System.out.println(resultString);
	}

}
