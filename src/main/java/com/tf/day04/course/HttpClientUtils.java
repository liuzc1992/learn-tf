package apitest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class HttpClientUtils {

//	public static String doGet(String url, Map<String, String> headers) {
//		
//	}

	public static String doGet(String url) {
		return doGet(url, null);
	}

	private static boolean isEmpty(final CharSequence cs) {
		return cs == null || cs.length() == 0;
	}

	// header 例如 token=11;user=123;
	public static String doGet(String url, String headers) {
		// 创建一个请求连接管理
		CloseableHttpClient httpclient = HttpClients.createDefault();
		// 请求
		HttpGet get = new HttpGet(url);
		// 设置代理
		HttpHost proxy = new HttpHost("127.0.0.1", 8888);
		RequestConfig requestConfig = RequestConfig.custom().setProxy(proxy).build();
		get.setConfig(requestConfig);
		// 处理头部信息
		if (!isEmpty(headers)) {
			String[] header_array = headers.split(";");
			for (String line : header_array) {
				String[] header = line.split("=");
				get.addHeader(header[0], header[1]);
			}
//			get.addHeader("token", "1212");	
//			get.addHeader("user", "1212");	
		}
		// HttpGet("http://118.24.13.38:8080/goods/UserServlet?method=loginMobile&loginname=test1&loginpass=test1");
		// 获取返回对象
		String result = "";
		CloseableHttpResponse resp;

		try {
			resp = httpclient.execute(get);
			// http 状态 200 404 302 500
			StatusLine line = resp.getStatusLine();
			System.out.println(line);
			// 结果
			HttpEntity httpEntity = resp.getEntity();
			// 结果转换
			result = EntityUtils.toString(httpEntity, "utf-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
//	public static String doPost(String url,String parameters, String headers) {
//		
//	}
	public static String doPost(String url,String parameters, String headers) {
		// 创建一个请求连接管理
		CloseableHttpClient httpclient = HttpClients.createDefault();
		// 请求
		HttpPost post = new HttpPost(url);
		//post 数据处理
		List<NameValuePair> formparams = new ArrayList<NameValuePair>();  
		formparams.add(new BasicNameValuePair("method", "loginMobile"));  
		formparams.add(new BasicNameValuePair("loginname", "test1"));  
		formparams.add(new BasicNameValuePair("loginpass", "test1"));  
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);  
		post.setEntity(entity);

		// 设置代理
		HttpHost proxy = new HttpHost("127.0.0.1", 8888);
		RequestConfig requestConfig = RequestConfig.custom().setProxy(proxy).build();
		post.setConfig(requestConfig);
		// 处理头部信息
		if (!isEmpty(headers)) {
			String[] header_array = headers.split(";");
			for (String line : header_array) {
				String[] header = line.split("=");
				post.addHeader(header[0], header[1]);
			}
//			get.addHeader("token", "1212");	
//			get.addHeader("user", "1212");	
		}
		// HttpGet("http://118.24.13.38:8080/goods/UserServlet?method=loginMobile&loginname=test1&loginpass=test1");
		// 获取返回对象
		String result = "";
		CloseableHttpResponse resp;

		try {
			resp = httpclient.execute(post);
			// http 状态 200 404 302 500
			StatusLine line = resp.getStatusLine();
			System.out.println(line);
			// 结果
			HttpEntity httpEntity = resp.getEntity();
			// 结果转换
			result = EntityUtils.toString(httpEntity, "utf-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	
	public static void main(String[] args) {
		doPost("http://118.24.13.38:8080/goods/UserServlet","user=12&id=12","user=123");
	}
}
