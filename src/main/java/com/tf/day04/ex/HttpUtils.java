package com.tf.day04.ex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.alibaba.druid.stat.TableStat.Name;


//若还需要加header  加证书？   加乱码调整
public class HttpUtils {
	public static String doGet(String url) {
			return doGet(url,null);
	}
	
	public static String doGet(String url,String headers) {
		// 创建一个请求连接管理
		CloseableHttpClient httpClient = HttpClients.createDefault();
		//进行http请求
		HttpGet get = new HttpGet(url);
		CloseableHttpResponse resp = null;
		String result = null;
		StatusLine line = null;
		



		
		//设置代理???
		HttpHost proxy = new HttpHost("127.0.0.1", 8888);
		RequestConfig requestConfig = RequestConfig.custom().setProxy(proxy).build();
		get.setConfig(requestConfig);
		
		//处理头部信息
		if (headers!=null) {
			String[] header_array =headers.split(";");
			for(String line1:header_array) {
			String[] header=line1.split("=");
			get.addHeader(header[0], header[1]);
			}
		}
		try {
			// 获取返回对象
			resp = httpClient.execute(get);
			//http 状态码  200,302,404,500
			line = resp.getStatusLine();
			//结果
			HttpEntity httpEntity = resp.getEntity();
			//结果转换
			result = EntityUtils.toString(httpEntity,"utf-8");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "状态码是：" + line + "\r\n" + "响应结果是：" + result;
	}

	
	public static String doPost(String url,String parameters,String headers) {
		// 创建一个请求连接管理
		CloseableHttpClient httpClient = HttpClients.createDefault();
		//进行http请求
		HttpGet post = new HttpGet(url);
		CloseableHttpResponse resp = null;
		String result = null;
		StatusLine line = null;
		
		List<NameValuePair> formparams = new ArrayList<NameValuePair>();  
		formparams.add(new BasicNameValuePair("param1", "value1"));  
		formparams.add(new BasicNameValuePair("param2", "value2"));  
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);  
		HttpPost httppost = new HttpPost("http://localhost/handler.do");  
		httppost.setEntity(entity);          
//		System.out.println(httppost.getURI());  
		
		//设置代理???
		HttpHost proxy = new HttpHost("127.0.0.1", 8888);
		RequestConfig requestConfig = RequestConfig.custom().setProxy(proxy).build();
		post.setConfig(requestConfig);
		
		//处理头部信息
		if (headers!=null) {
			String[] header_array =headers.split(";");
			for(String line1:header_array) {
			String[] header=line1.split("=");
			post.addHeader(header[0], header[1]);
			}
		}
		
		
		
		try {
			// 获取返回对象
			resp = httpClient.execute(post);
			//http 状态码  200,302,404,500
			line = resp.getStatusLine();
			//结果
			HttpEntity httpEntity = resp.getEntity();
			//结果转换
			result = EntityUtils.toString(httpEntity,"utf-8");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "状态码是：" + line + "\r\n" + "响应结果是：" + result;
	}

}
