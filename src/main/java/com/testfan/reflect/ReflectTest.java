package com.testfan.reflect;

import java.lang.reflect.Field;

import org.apache.commons.beanutils.BeanUtils;

import com.tf.day03.courseInfo.DbUser;

public class ReflectTest {
	
	public static void main(String[] args) {
		//传统构造方式
		//DbUser dbUser = new DbUser();
		//根据类的路径 匹配个对象
		try {
//			//class 获取方式1
//			Class clzClass = Class.forName("com.testfan.db.DbUser");
//			//class 获取方式2 
//			Class clzClass2= DbUser.class;
//			//class 获取方式3
//			Object object = clzClass.newInstance();
//			Class clzClass3 = object.getClass();
//			if(object instanceof DbUser) {
//				System.out.println(" ok ");
//			}
//			Field[] fields= clzClass3.getDeclaredFields();
//			for (Field field : fields) {
//				System.out.println(field.getName());
//			}
			
//			Field field = clzClass3.getDeclaredField("id");
//			field.setAccessible(true);
//			//对象
//			field.set(object, "test");
//			System.out.println(object);
			DbUser dbUser = new com.tf.day03.courseInfo.DbUser();
			BeanUtils.setProperty(dbUser, "id", "test");
			System.out.println(dbUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
