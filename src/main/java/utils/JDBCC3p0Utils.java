package utils;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class JDBCC3p0Utils {

    private static DataSource ds = new ComboPooledDataSource();

    /**
     * 通过c3p0获取数据库连接
     * @return
     * @throws SQLException
     */
    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    /**
     * 获取数据连接池
     * @return
     */
    public static DataSource getDataSource() {
        return ds;
    }

    public static void main(String[] args) throws SQLException {
        System.out.println(getConnection());
    }

}
